module.exports = function(server) {
  var io = require('socket.io')(server);

  // Socket.io tutorial code for real-time chat.
  io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('chat message', function(msg){
      io.emit('chat message', msg);
    });
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });
  });
};
