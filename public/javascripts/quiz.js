$(document).ready(function() {
  (function(Welcome) {
    // ----------------------------------------------------------------------------------------------------
    // Private constants.


    var SELECTORS = {
      hiddenVideoArea: ".lower-edge > div",
      hideBtn: "#hide_btn",
      revealBtn: "#reveal_btn",
      videoBox: "#video_box",
      videoContainer: "#video_container",
    };


    var SAMPLE_VIDEO_DATA = {
      animeName: "Tokyo Ravens",
      songName: "Break a Spell",
      songDescription: "ED 2",
      videoId: "FedDpFKew1c", // Tokyo Ravens ED 2: Break a Spell
    };


    // ----------------------------------------------------------------------------------------------------
    // Private functions.


    var playHiddenVideo = function(videoId) {
      var $videoContainer = $(SELECTORS.videoContainer);
      var $hiddenVideoArea = $(SELECTORS.hiddenVideoArea);
      $videoContainer.empty();
      $videoContainer.detach();
      $hiddenVideoArea.append($videoContainer);
      Video.embedYouTubeVideo(videoId, $videoContainer);
    };


    var hideVideo = function() {
      var $videoContainer = $(SELECTORS.videoContainer);
      var $hiddenVideoArea = $(SELECTORS.hiddenVideoArea);
      $videoContainer.detach();
      $hiddenVideoArea.append($videoContainer);
    };


    var revealVideo = function() {
      var $videoContainer = $(SELECTORS.videoContainer);
      var $videoBox = $(SELECTORS.videoBox);
      $videoContainer.detach();
      $videoBox.append($videoContainer);
    };


    // ----------------------------------------------------------------------------------------------------
    // Code to execute immediately.


    (function() {
      playHiddenVideo(SAMPLE_VIDEO_DATA.videoId);

      // Listen for events.
      $(SELECTORS.hideBtn).click(hideVideo);
      $(SELECTORS.revealBtn).click(revealVideo);
    })();


  })(window.Welcome = window.Welcome || {});
});
