$(document).ready(function() {
  (function(Video) {
    // ----------------------------------------------------------------------------------------------------
    // Private constants.


    // Pug template function names.
    var PUGS = {
      youtubeVideo: "youtubeVideoTemplate"
    };


    // ----------------------------------------------------------------------------------------------------
    // Public functions.


    Video.embedYouTubeVideo = function(videoId, $elem) {
      context = {
        videoId: videoId,
      };
      $elem.html(window[PUGS.youtubeVideo](context));
    };


  })(window.Video = window.Video || {});
});
