$(function () {
  // Socket.io tutorial code for real-time chat.
  var socket = io();

  $('form').submit(function(){
    socket.emit('chat message', $('#chat_msg_input').val());
    $('#chat_msg_input').val('');
    return false;
  });

  socket.on('chat message', function(msg){
    var $chatMessages = $('#chat_messages');
    var $newMsg = $('<li>').text(msg);
    $chatMessages.append($newMsg);

    // Scroll to new message.
    $chatMessages.animate({
      scrollTop: $newMsg.offset().top - $chatMessages.offset().top + $chatMessages.scrollTop()
    });
  });
});
