# Better Music Quiz (BMQ) Readme #


## How to run BMQ on your local machine ##

Below are the instructions to run the BMQ webserver on your local machine.

(last updated: 2018 April 02)


### Install Node and NPM ###

If you do not already have Node and NPM installed on your system, please look-up and follow a tutorial or guide online to install them.

For example, Mozilla has a useful guide called Setting up a Node development environment:
https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/development_environment


### Clone the BMQ repository ###

First, install Git on your system if you do not have it already.

Next, clone the repository:

* Run `git clone https://<YourName>@bitbucket.org/bettermusicquiz/better-music-quiz.git` where <YourName> is your BitBucket username.
* If the repository is private, you will have to ask a BMQ dev to grant you access on BitBucket first.
* If the repository is public, you may be able to clone it without a BitBucket account by running `git clone https://bitbucket.org/bettermusicquiz/better-music-quiz.git`


### Install NPM packages ###

In your command line (e.g. Windows PowerShell, Terminal on OSX, etc.), navigate to your BMQ website directory.

Run `npm install`.


### Install Heroku CLI ###

If you plan to push your changes directly to a Heroku server, then you must install the Heroku CLI.
As usual, you can find Heroku's CLI installation guide via Google search.


### Create your .env file ###

The .env file contains environment variables that will be set each time you start the server.
It may contain configuration options, API keys, etc.

To create a .env file, first copy the `example.env` file to a new file called `.env`.
Next, open the `.env` file in a text editor and fill-in any required environment variables that have no value yet, e.g. API keys.
(Early on, there may be no such environment variables.)


### Run the server ###

In your BMQ website directory, you can start the server with any of the following commands:

* Run `npm start`. This is the normal way to start the server.
* Run `npm run devstart`. This starts nodemon, which will automatically restart your server whenever you make code changes.
* Run `heroku local web`. This should be the same as running `npm start` and verifies your server will run correctly on Heroku.
  Naturally, this will work only if you have the Heroku CLI installed.
  The contents of Procfile determine what this command will do.

Once you've started the server, visit `http://http://localhost:3000/` in your web browser.
(The port number should match the value of PORT in your `.env` file.)

That's it!
